
ROS_MELODIC_TAG=amarburg/drone-ci-ros-melodic
ROS_NOETIC_TAG=amarburg/drone-ci-ros-noetic
ROS_FOXY_TAG=amarburg/drone-ci-ros-foxy

all: build push

build: drone-ci-melodic-ros-base drone-ci-noetic-ros-base drone-ci-foxy-ros-base

force: force-melodic force-noetic force-foxy

push:
	docker push $(ROS_MELODIC_TAG):latest
	docker push $(ROS_NOETIC_TAG):latest
	docker push $(ROS_FOXY_TAG):latest


## == Melodic images ==
drone-ci-melodic-ros-base:
	docker build -t $(ROS_MELODIC_TAG):latest --file melodic/Dockerfile.melodic-ros-base melodic

force-melodic:
	docker build --no-cache  -t $(ROS_MELODIC_TAG):latest --file melodic/Dockerfile.melodic-ros-base melodic

## == Noetic images ==
drone-ci-noetic-ros-base:
	docker build -t $(ROS_NOETIC_TAG):latest --file noetic/Dockerfile.noetic-ros-base noetic

force-noetic:
	docker build --no-cache  -t $(ROS_NOETIC_TAG):latest --file noetic/Dockerfile.noetic-ros-base noetic

## == Foxy images ==
drone-ci-foxy-ros-base:
	docker build -t $(ROS_FOXY_TAG):latest --file foxy/Dockerfile.foxy-ros-base foxy

force-foxy:
	docker build --no-cache  -t $(ROS_FOXY_TAG):latest --file foxy/Dockerfile.foxy-ros-base foxy


.PHONY: build push \
			force-melodic drone-ci-melodic-ros-base \
			force-noetic drone-ci-noetic-ros-base \
                        force-foxy drone-ci-foxy-ros-base
